
export type Type = string;

interface InputDescription {
  name: string;
  type: Type;
}

interface OutputDescription {
  name: string;
  type: Type;
}

interface NodeDescription {
  name: string;
  inputs: InputDescription[];
  outputs: OutputDescription[];
}

interface NodeInstance {
  name: string;
  uuid: number;
  description: NodeDescription;
}
