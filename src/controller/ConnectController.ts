import { ConnectorId, EdgeId, IViewActions } from "../state/ViewStore";
import { MouseButton } from "../input/ClickRecognizer";
import { imVec2 } from "../core/vec";

export class ConnectController {
  constructor(private element: Element, private state: ConnectController.State, private actions: ConnectController.Actions) {}

  updateState(state: ConnectController.State, actions: ConnectController.Actions) {
    this.state = state;
    this.actions = actions;
  }

  handleConnectorClick(connectorId: ConnectorId, connectorType: "in" | "out", button: MouseButton) {
    if (button !== MouseButton.Left) return;

    (this.element as any).focus();

    // start the connection
    if (!this.state.startConnector) {
      this.actions.start(connectorId, connectorType);
      this.element.addEventListener("pointermove", this.handlePointerMove);
      this.element.addEventListener("keydown", this.handleKey);
    }
    // don't allow to connect the two same connectors
    else if (this.state.startConnector === connectorId) {
      return;
    }
    // don't allow to connect two inputs or outputs
    else if (this.state.startType === connectorType) {
      return;
    }
    // finish the connection
    else {
      this.actions.finish(connectorId);
    }
  }

  private handlePointerMove = (ev: PointerEvent) => {
    this.actions.update([ev.x, ev.y]);
  }

  private handleKey = (ev: KeyboardEvent) => {
    if (ev.key === "Escape") {
      this.actions.reset();
    }
  }
}

export namespace ConnectController {
  export interface State {
    startConnector?: ConnectorId; // Start point of the connection attempt.
    startType?: "in" | "out"; // Whether we started at an input or output.
    tmpEndConnector?: ConnectorId; // Temporary controller under the cursor. Endpoint for temporary edge.
    tmpEdge?: EdgeId; // Temporary edge displayed while a connection attempt is active.
  }

  export interface Actions {
    start(connectorId: ConnectorId, connectorType: "in" | "out"): void;
    update(position: imVec2): void;
    reset(): void;
    finish(end: ConnectorId): void;
  }
}
