import { IViewActions, IViewQueries, EdgeId, IViewState } from "../state/ViewStore";

export class EdgeHoverController {
  constructor(private element: Element, private state: IViewState, private actions: IViewActions, private queries: IViewQueries) {
    this.element.addEventListener("pointermove", this.handlePointerMove.bind(this));
    this.element.addEventListener("keydown", this.handleKey);
  }

  updateState(state: IViewState, actions: IViewActions) {
    this.state = state;
    this.actions = actions;
  }

  private handlePointerMove(ev: PointerEvent) {
    if (this.state.controllers.connect.startConnector) {
      this.actions.updateEdgeHover();
    } else {
      const edge = this.queries.intersectTopmostEdge([ev.x, ev.y]); 
      this.actions.updateEdgeHover(edge);
    }
  }

  private handleKey = (ev: KeyboardEvent) => {
    if (ev.key === "Delete") {
      const edge = this.state.controllers.edgeHover.edge;
      if (edge) {
        this.actions.updateEdgeHover();
        this.actions.destroyEdge(edge);
      }
    }
  }
}

export namespace EdgeHoverController {
  export interface State {
    edge?: EdgeId;
  }
}
