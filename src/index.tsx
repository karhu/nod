import * as React from "react";
import * as ReactDOM from "react-dom";

import { View } from "./components/View";
import { ViewStore } from "./state/ViewStore";

const nodes = [
  { title: "first-node" , dimension: [300, 100], position: [10, 100] },
  { title: "second-node" , dimension: [200, 400], position: [500, 10] }
]

const store = new ViewStore();

const nodeValue5 = store.createNode("5", [50, 200], [0, 0], [
], [
  { name: "value" },
]);

const nodeValue4 = store.createNode("4", [50, 400], [0, 0], [
], [
  { name: "value" },
]);

const nodeAdd = store.createNode("Add", [300, 300], [0, 0], [
  { name: "a" },
  { name: "b" }
], [
  { name: "result" },
]);

const nodeDouble = store.createNode("x2", [300, 100], [0, 0], [
  { name: "a" },
], [
  { name: "result" },
]);

const nodeMultiply = store.createNode("Mult", [600, 200], [0, 0], [
  { name: "a" },
  { name: "b" }
], [
  { name: "result" },
]);

const nodeConsoleLog = store.createNode("console.log", [800, 200], [0, 0], [
  { name: "a" },
], [
]);

store.createEdge(store.getConnector(nodeValue4, false, 0), store.getConnector(nodeAdd, true, 1));
store.createEdge(store.getConnector(nodeValue5, false, 0), store.getConnector(nodeAdd, true, 0));
store.createEdge(store.getConnector(nodeValue5, false, 0), store.getConnector(nodeDouble, true, 0));
store.createEdge(store.getConnector(nodeDouble, false, 0), store.getConnector(nodeMultiply, true, 0));
store.createEdge(store.getConnector(nodeAdd, false, 0), store.getConnector(nodeMultiply, true, 1));
store.createEdge(store.getConnector(nodeMultiply, false, 0), store.getConnector(nodeConsoleLog, true, 0));

(window as any).store = store;

let dirty = false;

function render() {
  dirty = false;
  ReactDOM.render(
    <View data={store.state} actions={{ view: store, connectController: store.connectControllerActions}} queries={store} />,
    document.getElementById("example")
  );
}

function setDirty() {
  if (!dirty) {
    dirty = true;
    requestAnimationFrame(render);
  }
}

store.onChange = setDirty;

setDirty();