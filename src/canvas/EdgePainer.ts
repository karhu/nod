import { imVec2 } from "../core/vec";

type Context = CanvasRenderingContext2D;

interface RenderOptions {
  highlighted: boolean;
  width: number;
}

export class EdgePainter {
  begin(ctx: Context): void {
    this.ctx = ctx;
    this.ctx.save();
  }

  end(): void {
    this.ctx.restore();
  }

  render(start: imVec2, end: imVec2, startD: imVec2, endD: imVec2, options: RenderOptions): void {}

  protected ctx: Context;
}

export class FirstEdgePainter extends EdgePainter {
  begin(ctx: Context) {
    super.begin(ctx);

    ctx.lineWidth = 8;
    ctx.strokeStyle = 'rgba(0, 0, 0, 0.1)';
    ctx.lineCap = "round";

    ctx.shadowBlur = 8;
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 2;
  }
  
  render(start: imVec2, end: imVec2, startD: imVec2, endD: imVec2, options: RenderOptions) {
    const ctx = this.ctx;
    const posA = start;
    const posB = end;
  
    ctx.lineWidth = 4;
    ctx.strokeStyle = "rgba(50, 100, 200, 1.0)";
    ctx.shadowColor = "rgba(0, 0, 0, 0.0)";

    ctx.beginPath();
    ctx.moveTo(posA[0], posA[1]);
    ctx.bezierCurveTo(
      posA[0] + startD[0], posA[1] + startD[1],
      posB[0] +   endD[0], posB[1] +   endD[1], 
      posB[0]            , posB[1]
    );
    ctx.stroke();
  }
}

export class SecondEdgePainter extends EdgePainter {
  begin(ctx: Context) {
    super.begin(ctx);

    ctx.lineCap = "round";

    ctx.shadowBlur = 8;
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 2;
  }
  
  render(start: imVec2, end: imVec2, startD: imVec2, endD: imVec2, options: RenderOptions) {
    const ctx = this.ctx;
    const posA = start;
    const posB = end;

    ctx.lineWidth = options.width;
    ctx.shadowColor = "rgba(0, 0, 0, 0.5)";

    if (options.highlighted) {
      ctx.strokeStyle = "rgba(70, 140, 256, 1.0)";
    } else {
      ctx.strokeStyle = "rgba(50, 100, 200, 1.0)";
    }

    ctx.beginPath();
    ctx.moveTo(posA[0], posA[1]);
    ctx.bezierCurveTo(
      posA[0] + startD[0], posA[1] + startD[1],
      posB[0] +   endD[0], posB[1] +   endD[1], 
      posB[0]            , posB[1]
    );
    ctx.stroke();
  
    // ctx.lineWidth = 4;
    // ctx.strokeStyle = "rgba(50, 100, 200, 1.0)";
    // ctx.shadowColor = "rgba(0, 0, 0, 0.0)";

    // ctx.beginPath();
    // ctx.moveTo(posA[0], posA[1]);
    // ctx.bezierCurveTo(
    //   posA[0] + startD[0], posA[1] + startD[1],
    //   posB[0] +   endD[0], posB[1] +   endD[1], 
    //   posB[0]            , posB[1]
    // );
    // ctx.stroke();
  }
}