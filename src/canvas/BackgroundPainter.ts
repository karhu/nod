type Context = CanvasRenderingContext2D;

export class BackgroundPainter {
  render(ctx: Context, width: number, height: number): void {
    this.ctx = ctx;
    this.ctx.save();
  }

  protected ctx: Context;
}

export class GridBackgroundPainter extends BackgroundPainter {
  render(ctx: Context, width: number, height: number): void {
    ctx.save();
    
    ctx.clearRect(0, 0, width, height);

    const style1 = "rgba(0, 0, 0, 0.1)";
    const style2 = "rgba(255, 255, 255, 0.01)";

    ctx.lineWidth = 1.0;
    ctx.strokeStyle = style2;

    const renderGrid = (spacing: number) => {
      let x = 0.5; 
      let y = 0.5;

      ctx.beginPath();
      while (x < width) {
        ctx.moveTo(x, 0);
        ctx.lineTo(x, height);
        x += spacing;
      }
      while (y < height) {
        ctx.moveTo(0, y);
        ctx.lineTo(width, y);
        y += spacing;
      }
      ctx.stroke();
    }
    
    renderGrid(30);
    renderGrid(150);

    ctx.restore();
  }
}