type SubscriptionCallback<T> = (state: T) => void;
type SubscriptionId = number;

export abstract class Store<T> {
  subscribe(cb: SubscriptionCallback<T>): void {
    const id = this.nextId++;
    this.onChangeListeners.set(id, cb);
  }

  unsubscribe(id: SubscriptionId): void {
    this.onChangeListeners.delete(id);
  }

  protected abstract get state(): T;
  protected abstract set state(v: T);

  protected notify(): void {
    this.onChangeListeners.forEach(cb => cb(this.state));
  }

  private onChangeListeners = new Map<SubscriptionId, SubscriptionCallback<T>>();
  private nextId = 1;
}
