
import Bezier = require("bezier-js");
import { IViewState, EdgeId, IConnector } from "./ViewStore";

export class BezierCache {
  get(state: IViewState, edgeId: EdgeId): Bezier | null {
    const edge = state.edges.get(edgeId);
    const begin = state.connectors.get(edge.begin);
    const end = state.connectors.get(edge.end);

    // skip edges that have no specified begin and end
    if (!begin.position || !end.position || !begin.direction || !end.direction) {
      return null;
    }

    // cache hit and data not outdated
    const entry = this.cache.get(edgeId);
    if (entry && entry.begin === begin && entry.end === end) {
      return entry.bezier;
    }

    // create new entry
    const bezier = new Bezier(
      begin.position[0], begin.position[1],
      begin.position[0] + begin.direction[0], begin.position[1] + begin.direction[1],
      end.position[0] + end.direction[0], end.position[1] + end.direction[1],
      end.position[0], end.position[1]
    );
    this.cache.set(edgeId, { begin, end, bezier });
    return bezier;
  }

  private cache = new Map<EdgeId, CacheEntry>();
}

interface CacheEntry {
  bezier: Bezier;
  begin: IConnector;
  end: IConnector;
}