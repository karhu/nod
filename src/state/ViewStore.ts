import { vec2, imVec2 } from "../core/vec";
import { BezierCache } from "./BezierCache";
import { EdgeHoverController } from "../controller/EdgeHoverController";
import { ConnectController } from "../controller/ConnectController";
import { ConnectControllerStore } from "./ConnectControllerStore";

// type primitive = string | number | boolean | undefined | null;

// type DeepReadonly<T> = T extends primitive ? T : DeepReadonlyObject<T>;

// type DeepReadonlyObject<T> = {
//   readonly [P in keyof T]: DeepReadonly<T[P]>
// }

export type NodeId = number;
export type EdgeId = number;
export type EdgeAnchorId = number;
export type ConnectorId = number;

export interface INodeDisplayProps {
  dimension: vec2;
  position: vec2;
}

export interface INode {
  id: NodeId;
  title: string;
  display: INodeDisplayProps;
  inputConnectors: Array<IInputOutput>;
  outputConnectors: Array<IInputOutput>;
}

export interface IEdge {
  id: EdgeId;
  begin: ConnectorId;
  end: ConnectorId;
}

export interface IConnector {
  id: ConnectorId;

  position?: imVec2;
  direction?: imVec2;
}

export interface IViewState {
  nodes: Map<NodeId, INode>;
  edges: Map<EdgeId, IEdge>;
  connectors: Map<ConnectorId, IConnector>;

  controllers: IControllerState
  style: IViewStyle;
}

export interface IViewStyle {
  edgeWidth: number;
}

export interface IControllerState {
  edgeHover: EdgeHoverController.State;
  connect: ConnectController.State;
}

export interface IInputOutputDecl {
  name: string
}

export interface IInputOutput {
  connectorId: ConnectorId;
  name: string
  offset: imVec2 | null;
}

export interface IViewActions {
  createNode(title: string, position: vec2, dimension: vec2, inputs: IInputOutputDecl[], outputs: IInputOutputDecl[]): NodeId;
  moveNode(id: NodeId, newPos: vec2): void;
  setNodeConnectorOffset(id: NodeId, isInput: boolean, idx: number, offset: imVec2): void;

  createEdge(begin: ConnectorId, end: ConnectorId): EdgeId;
  destroyEdge(id: EdgeId): void;

  createConnector(direction?: imVec2, position?: imVec2): ConnectorId;
  destroyConnector(id: ConnectorId): void;
  updateConnectorPosition(id: ConnectorId, position: imVec2): void;

  updateEdgeHover(edge?: EdgeId): void;
}

export interface IViewQueries {
  getConnector(node: NodeId, isInput: boolean, index: number): ConnectorId;
  intersectTopmostEdge(position: imVec2): EdgeId | null;
}

const EDGEDIR_LEN = 100;

export class ViewStore implements IViewActions, IViewQueries {

  constructor() {
    this._connectControllerStore = new ConnectControllerStore(this);
    this._connectControllerStore.subscribe((state) => {
      this._state = { 
        ...this.state, 
        controllers: {
          ...this.state.controllers,
          connect: state
        }
      };
      this.notify();
    })
  }

  createNode(title: string, position: vec2, dimension: vec2, inputs: IInputOutputDecl[], outputs: IInputOutputDecl[]) {
    const id = this.generateId();

    const inputConnectors = new Array<IInputOutput>();
    const outputConnectors = new Array<IInputOutput>();
    const inCount = inputs.length;
    const outCount = outputs.length;

    const dIn = dimension[1] / (inCount + 1);
    for (let i=0; i<inCount; i++) {
      const connectorId = this.createConnector([-EDGEDIR_LEN, 0]);
      inputConnectors.push({ connectorId, name: inputs[i].name, offset: null});
    }

    const dOut = dimension[1] / (outCount + 1);
    for (let i=0; i<outCount; i++) {
      const connectorId = this.createConnector([EDGEDIR_LEN, 0]);
      outputConnectors.push({ connectorId, name: outputs[i].name, offset: null});
    }

    this._state.nodes = this._state.nodes.set(id, {
      id,
      title,
      display: {
        dimension,
        position
      },
      inputConnectors,
      outputConnectors
    });

    this.notify();
    return id;
  }

  moveNode(id: NodeId, newPos: vec2) {
    this._state.nodes.forEach((n) => {
      if (n.id === id) {
        n.display.position = newPos;

        n.inputConnectors.forEach((ioc) => {
          this.updateConnectorPosition(ioc.connectorId, imVec2.add(newPos, ioc.offset));
        });

        n.outputConnectors.forEach((ioc) => {
          this.updateConnectorPosition(ioc.connectorId, imVec2.add(newPos, ioc.offset));
        });
      }
    });

    this.notify();
  }

  createConnector(direction?: imVec2, position?: imVec2): ConnectorId {
    const id = this.generateId();
    this._state.connectors.set(id, { id, direction, position });
    this.notify();
    return id;
  }

  destroyConnector(id: ConnectorId) {
    this._state.connectors.delete(id);
    this.notify();
  }

  updateConnectorPosition(id: ConnectorId, position: imVec2) {
    const connector = this._state.connectors.get(id);
    this._state.connectors.set(id, { ...connector, position });
    this.notify();
  }

  createEdge(begin: ConnectorId, end: ConnectorId): EdgeId {
    const id = this.generateId();

    this.state.edges.set(id, {
      id,
      begin, end
    });

    this.notify();
    return id;
  }

  destroyEdge(id: EdgeId): void {
    this.state.edges.delete(id);
    this.notify();
  }

  updateEdgeHover(edge?: EdgeId): void {
    if (this.state.controllers.edgeHover.edge === edge) return;

    this.state.controllers.edgeHover = {
      ...this.state.controllers.edgeHover, edge
    }
    this.notify();
  }

  setNodeConnectorOffset(id: NodeId, isInput: boolean, idx: number, offset: imVec2): void {
    const node = this.state.nodes.get(id);
    const io = isInput ? node.inputConnectors[idx] : node.outputConnectors[idx];
    io.offset = offset;

    this.updateConnectorPosition(io.connectorId, imVec2.add(node.display.position, io.offset));
    this.notify();
  }

  ////

  get state(): IViewState {
    return this._state;
  }

  get connectControllerActions(): ConnectController.Actions {
    return this._connectControllerStore;
  }

  getConnector(nodeId: NodeId, isInput: boolean, index: number): ConnectorId {
    const node = this.state.nodes.get(nodeId);
    const conn = isInput ? node.inputConnectors[index] : node.outputConnectors[index];
    return conn.connectorId;
  }

  intersectTopmostEdge(position: imVec2): EdgeId | null {
    const x = position[0];
    const y = position[1];

    const dist = this.state.style.edgeWidth / 2;
    const distSquared = dist * dist;

    const edgeIds = Array.from(this.state.edges.keys()).reverse();

    for (const edgeId of edgeIds) {
      const bezier = this._bezierCache.get(this._state, edgeId);
      if (!bezier) continue;

      const bbox = bezier.bbox();
      if (x > bbox.x.max || x < bbox.x.min || y > bbox.y.max || y < bbox.y.min) continue;

      const pt = bezier.project({x, y});
      const curvePoint: imVec2 = [pt.x, pt.y];
      if (imVec2.distanceSquared(curvePoint, position) < distSquared) {
        return edgeId;
      }
    }

    return null;
  }

  ///

  onChange = () => {};

  private _state: IViewState = {
    nodes: new Map(),
    edges: new Map(),
    connectors: new Map(),
    style: {
      edgeWidth: 7
    },
    controllers: {
      edgeHover: {},
      connect: {}
    }
  };

  private _bezierCache = new BezierCache();
  private _connectControllerStore:ConnectControllerStore;

  private generateId(): number {
    return this._nextId++;
  }

  private notify() {
    this.onChange();
  }

  private _nextId = 1;
}

const PICK_RADIUS_SQUARED = 7*7;





