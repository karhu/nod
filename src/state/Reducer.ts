// Action

type ActionType = string;

interface IAction {
  type: ActionType;
}

interface Action<Type extends ActionType, Payload> extends IAction {
  type: Type;
  payload: Payload;
}

interface ActionDeclarationBuilder<Type extends ActionType> {
  empty(): ActionDeclarationEmpty<Type>;
  payload<Payload>(): ActionDeclaration<Type, Payload>;
}

type ActionDeclarationEmpty<Type extends ActionType> = (() => Action<Type, undefined>) & { type: Type };
type ActionDeclaration<Type extends ActionType, Payload> = ((p: Payload) => Action<Type, Payload>) & { type: Type };

function declareAction<Type extends ActionType>(type: Type): ActionDeclarationBuilder<Type> {
  return {
    empty: () => {   
      const fn = () => ({ type });
      return Object.assign(fn, { type });
    },
    payload: () => {
      const fn = (p: any) => ({ type, payload: p });
      return Object.assign(fn, { type });
    }
  } as any;
}

// Reducer

// interface Reducer<State, Type extends string, Payload> {
//   call(state: State, action: Action<Type, Payload>): State;
// }

type ReducerCallable<State, Actions> = (state: State, action: Actions) => State;

type Reducer<State, Actions> = ReducerCallable<State, Actions> & {
  actionTypes: Set<ActionType>;
};

function createReducer<State, Type extends string, Payload>(
  actionDec: { type: Type }, 
  fn: (state: State, payload: Payload) => State
) : Reducer<State, Action<Type, Payload>> {
  type ReducerT = Reducer<State, Action<Type, Payload>>;

  const red = ((state: State, action: Action<Type, Payload>) => {
    return action.type === actionDec.type ? fn(state, action.payload) : state;
  }) as ReducerT;
  red.actionTypes = new Set([actionDec.type]);

  return red;
}

// Reducer chaining

function chainReducers<State, A1, A2>(
  r1: Reducer<State, A1>, 
  r2: Reducer<State, A2>
): Reducer<State, A1 | A2>;
function chainReducers<State, A1, A2, A3>(
  r1: Reducer<State, A1>, 
  r2: Reducer<State, A2>, 
  r3: Reducer<State, A3>
): Reducer<State, A1 | A2 | A3>;
function chainReducers<State, A1, A2, A3, A4>(
  r1: Reducer<State, A1>, 
  r2: Reducer<State, A2>, 
  r3: Reducer<State, A3>, 
  r4: Reducer<State, A4>
): Reducer<State, A1 | A2 | A3 | A4>;
function chainReducers<State, Actions>(...reducers: Array<Reducer<State, Actions>>): Reducer<State, Actions> {
  type ReducerT = Reducer<State, Actions>;

  const red = ((state: State, action: Actions) => {
    return reducers.reduce((s, r) => r(s, action), state);
  }) as ReducerT;
  red.actionTypes = new Set();
  reducers.forEach(r => r.actionTypes.forEach(e => red.actionTypes.add(e)));

  return red;
}

// Reducer combining

type SecondArgument<Fun> = Fun extends (a: any) => any 
  ? void 
  : Fun extends (a: any, b: infer T) => any 
  ? T 
  : void;

type A = SecondArgument<(a: string) => void>;

type ReducersMap<State, Actions> = {
 [P in keyof State]: Reducer<State[P], Actions>
};

function combineReducers<State>(reducersMap: ReducersMap<State, IAction>) {
  type ReducerT = Reducer<State, IAction>;
  const keys = Object.keys(reducersMap).map((key: keyof State) => key);
  const reducers = keys.map(key => ({key, reducer: reducersMap[key]}) );

  const red = ((state: State, action: IAction) => {
    const newState = {} as State;
    let changed = false;
    keys.forEach(k => {
      newState[k] = reducersMap[k](state[k], action);
      changed = changed || newState[k] !== state[k];
    });
    return changed ? newState : state;
  }) as ReducerT;

  red.actionTypes = new Set();
  reducers.forEach(r => r.reducer.actionTypes.forEach(e => red.actionTypes.add(e)));
  return red;
}

// Test

interface TheState {
  foo: string; 
  counter: number;
}

const Increment = declareAction("INCREMENT").empty();
const Decrement = declareAction("DECREMENT").empty();

const r1 = createReducer(Increment, (state: TheState, payload) => {
  return { ...state, counter: state.counter + 1};
});

const r2 = createReducer(Decrement, (state: TheState, payload) => {
  return { ...state, counter: state.counter - 1};
});

const r12 = chainReducers(r1, r2);

interface SomeType {
  b: number;
  c: TheState;
}

let obj: SomeType = {
  b: 42, c: { foo: "bar", counter: 12}
};

const Duplicate = declareAction("DUPLICATE").empty();
const duplicateHandler = createReducer(Duplicate, (state: number, payload) => {
  return state * 2;
});

const comb = combineReducers({ b: duplicateHandler, c: r12 });
obj = comb(obj, Duplicate() );
