import { Store } from "./Store";
import { ConnectController } from "../controller/ConnectController";
import { ConnectorId, EdgeAnchorId, EdgeId } from "./ViewStore";
import { imVec2 } from "../core/vec";

interface RequiredActions {
  createEdge(begin: EdgeAnchorId, end: EdgeAnchorId): EdgeId;
  destroyEdge(id: EdgeId): void;
  createConnector(dir: imVec2, position: imVec2): ConnectorId;
  destroyConnector(id: ConnectorId): void;
  updateConnectorPosition(id: ConnectorId, position: imVec2): void;
}

export class ConnectControllerStore extends Store<ConnectController.State> implements ConnectController.Actions {
  constructor(protected actions: RequiredActions) {
    super();
  }

  start(connectorId: ConnectorId, connectorType: "in" | "out"): void {
    this.state = { ...this.state,
      startConnector: connectorId,
      startType: connectorType
    };
    this.notify();
  }

  update(position: imVec2): void {
    if (!this.state.startConnector) { return; }

    let tmpEndConnector = this.state.tmpEndConnector;
    if (!tmpEndConnector) {
      tmpEndConnector = this.actions.createConnector([0,0], position);
    } else {
      this.actions.updateConnectorPosition(tmpEndConnector, position);
    }

    let tmpEdge = this.state.tmpEdge;

    if (!tmpEdge) {
      tmpEdge = this.actions.createEdge(
        this.state.startConnector,
        tmpEndConnector
      );
    }

    this.state = { ...this.state,
      tmpEndConnector,
      tmpEdge
    }
    this.notify();
  }

  reset(): void {
    if (this.state.tmpEdge) {
      this.actions.destroyEdge(this.state.tmpEdge);
    }

    if (this.state.tmpEndConnector) {
      this.actions.destroyConnector(this.state.tmpEndConnector);
    }

    this.state = {};
    this.notify();
  }

  finish(end: ConnectorId): void {
    this.actions.createEdge(this.state.startConnector, end);
    this.reset();
  }

  protected state: ConnectController.State = {}
}