
export type vec2 = { [K in "0" | "1"]: number } & [number, number];
export type imVec2 = [number, number];

export namespace vec2 {
  export function add(t: vec2, a: imVec2, b: imVec2): vec2 {
    t[0] = a[0] + b[0];
    t[1] = a[1] + b[1];
    return t;
  }

  export function subtract(t: vec2, a: imVec2, b: imVec2): vec2 {
    t[0] = a[0] - b[0];
    t[1] = a[1] - b[1];
    return t;
  }

  export function distanceSquared(a: imVec2, b: imVec2): number {
    const dx = a[0] - b[0];
    const dy = a[1] - b[1];
    return dx*dx + dy*dy;
  }

  export function distance(a: imVec2, b: imVec2): number {
    return Math.sqrt(distanceSquared(a,b));
  }

  export function length(v: imVec2): number {
    return Math.sqrt(lengthSquared(v));
  }

  export function lengthSquared(v: imVec2): number {
    return v[0] * v[0] + v[1] * v[1];
  }
}

export namespace imVec2 {
  export function add(a: imVec2, b: imVec2): vec2 {
    const t0 = a[0] + b[0];
    const t1 = a[1] + b[1];
    return [t0, t1];
  }

  export function subtract(a: imVec2, b: imVec2): vec2 {
    const t0 = a[0] - b[0];
    const t1 = a[1] - b[1];
    return [t0, t1];
  }

  export function distanceSquared(a: imVec2, b: imVec2): number {
    const dx = a[0] - b[0];
    const dy = a[1] - b[1];
    return dx*dx + dy*dy;
  }

  export function scale(v: imVec2, factor: number): vec2 {
    const t0 = v[0] * factor;
    const t1 = v[1] * factor;
    return [t0, t1];
  }

  export function distance(a: imVec2, b: imVec2): number {
    return Math.sqrt(distanceSquared(a,b));
  }

  export function length(v: imVec2): number {
    return Math.sqrt(lengthSquared(v));
  }

  export function lengthSquared(v: imVec2): number {
    return v[0] * v[0] + v[1] * v[1];
  }
}