import { imVec2 } from "../core/vec";

export enum MouseButton {
  Left = 0,
  Middle = 1,
  Right = 2
}

export namespace ClickRecognizer {
  export interface Event { 
    position: imVec2;
    button: MouseButton;
  }

  export type Handler = (ev: Event) => void;
}

export class ClickRecognizer {
  private started: boolean = false;

  private handlePointerDown: any;
  private handlePointerUp: any;

  private downTimestamp: number;
  private downPosition: imVec2 = [0, 0];

  private handlers: Array<ClickRecognizer.Handler> = [];

  constructor(private element: Element, cb?: ClickRecognizer.Handler) {
    if (cb) {
      this.handlers.push(cb);
      this.start();
    }
  }

  start() {
    if (this.started) return;

    this.handlePointerDown = (ev: PointerEvent) => {
      this.downTimestamp = ev.timeStamp;
      this.downPosition = [ev.x, ev.y];
    }

    this.handlePointerUp = (ev: PointerEvent) => {
      const dt = ev.timeStamp - this.downTimestamp;
      if (dt > 400) return;

      const position: imVec2 = [ev.x, ev.y];

      const d2 = imVec2.distanceSquared(this.downPosition, position);
      if (d2 > 25) return;

      this.handlers.forEach(h => h({ position, button: ev.button }));
    }

    this.element.addEventListener("pointerdown", this.handlePointerDown);
    this.element.addEventListener("pointerup", this.handlePointerUp);

    this.started = true;
  }

  stop() {
    if (!this.started) return;

    this.element.removeEventListener("pointerdown", this.handlePointerDown);
    this.element.removeEventListener("pointerup", this.handlePointerUp);

    this.started = false;
  }

  addHandler(cb: ClickRecognizer.Handler) {
    this.handlers.push(cb);
    return this;
  }

  removeHandler(cb: ClickRecognizer.Handler) {
    this.handlers = this.handlers.filter(e => e !== cb);
    return this;
  }
}
