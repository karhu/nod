import * as React from "react";

import { Saturation as OriginalSaturation, Alpha } from "react-color/lib/components/common";
import { ColorResult, Color, CustomPicker, HSLColor, RGBColor } from "react-color";

export namespace ColorPicker {
  export interface Props {
    children?: any;
    color?: string;
    onChange?: (color: ColorResult) => void;
  }

  export interface State {
    color: Color;
  }
}

export class ColorPicker extends React.Component<ColorPicker.Props, ColorPicker.State> {
  constructor(props: ColorPicker.Props) {
    super(props);
    this.state = {
      color: props.color || "#888"
    };
  }

  render() {
    return <CustomColorPicker 
      onChange={(color: ColorResult) => {
        this.setState({ color: color.hex });
        if (this.props.onChange) {
          this.props.onChange(color);
        }
      }} 
      color={this.state.color}
      children={this.props.children}
    />
  }
};

const CustomColorPicker = CustomPicker((props: any) => {
  const children = props.children || []
  const childrenWithProps = React.Children.map(children, child =>
    React.cloneElement(child as any, { ...props, onChange: props.onChange })
  );

  return <div className="custom-color-picker">
    { childrenWithProps }
  </div> 
});

export const Saturation = (props: any) => {
  return <OriginalSaturation {...props} />
};

