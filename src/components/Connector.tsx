import * as React from "react";
import * as ReactDOM from "react-dom";
import { vec2, imVec2 } from "../core/vec";
import { IViewActions, ConnectorId} from "../state/ViewStore";
import { ClickRecognizer, MouseButton } from "../input/ClickRecognizer";

export enum ConnectorPosition {
  Left,
  Right
}

export interface ConnectorData {
  name: string;
  position: ConnectorPosition;
  index: number;
}

export interface Properties {
  actions: IViewActions;
  data: ConnectorData;
  onPositionChanged?: (pos: imVec2) => void;
  onClick?: (button: MouseButton) => void;
}

function createHookStyle(left: boolean): React.CSSProperties {
  const radius = 3;
  const border = 2;

  let hookStyle: React.CSSProperties = {
    width:  2 * radius,
    height: 2 * radius,
    borderRadius: 2 * radius,
    border: `${border}px solid rgba(50, 100, 200, 1.0)`,
    backgroundColor: `white`,
  };

  if (left) {
    hookStyle = {...hookStyle, 
      float: "left",
      marginRight: "0.2em",
      marginLeft: -(radius + border)
    }
  } else {
    hookStyle = {...hookStyle, 
      float: "right",
      marginLeft: "0.2em",
      marginRight: -(radius + border)
    }
  }

  return hookStyle;
}

function createNameStyle(): React.CSSProperties {
  return {
    fontSize: `0.9em`,
    color: `rgb(31, 31, 31)`
  };
}

export class Connector extends React.Component<Properties, {}> {

  private click: ClickRecognizer;

  private getPosition(): imVec2 {
    const thisDOM = ReactDOM.findDOMNode(this) as Element;
    const handles = thisDOM.getElementsByClassName("connectorHook");
    const rect =  handles[0].getBoundingClientRect();
    if (rect instanceof DOMRect) {
      return [rect.x + 0.5 * rect.width, rect.y + 0.5 * rect.height];
    }
  }

  componentDidMount() {
    if (this.props.onPositionChanged) {
      this.props.onPositionChanged(this.getPosition());
    }
    
    const domNode = ReactDOM.findDOMNode(this) as Element;
    const element = domNode.getElementsByClassName("connectorHook")[0];
    this.click = new ClickRecognizer(element, (ev) => {
      if (this.props.onClick) { 
        this.props.onClick(ev.button as MouseButton); 
      }
    });
  }

  componentDidUpdate() {
    if (this.props.onPositionChanged) {
      this.props.onPositionChanged(this.getPosition());
    }
  }

  shouldComponentUpdate(newProps: Properties) {
    return false;
  }

  render() {
    const data = this.props.data;
    const left = data.position === ConnectorPosition.Left;
  
    const hookStyle = createHookStyle(left);
    const nameStyle = createNameStyle();

    const wrapperStyle: React.CSSProperties = {
      display: `flex`,
      alignItems: `center`
    };
    
    if (left) {
      return <div style={wrapperStyle}> 
        <div className="connectorHook" style={hookStyle}></div> 
        <div style={nameStyle}>{data.name}</div>
      </div>
    } else {
      return <div style={wrapperStyle}> 
        <div style={nameStyle}>{data.name}</div>
        <div className="connectorHook" style={hookStyle}></div> 
      </div>
    }
  }
}
