import * as React from "react";

export interface DraggableHandleProps {
  children?: any;
}

export const DraggableHandle = (props: DraggableHandleProps) => {
  return <div className="draggable-handle draggable-cursor">
    {props.children}
  </div> 
}
