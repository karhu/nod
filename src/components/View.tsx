import * as React from "react";
import ResizeObserver from "resize-observer-polyfill";
import { Node, Properties as NodeProperties } from "./Node";
import { vec2, imVec2 } from "../core/vec";
import { IViewState, IViewActions, IViewQueries, ConnectorId, EdgeId } from "../state/ViewStore";
import ReactDOM = require("react-dom");
import { MouseButton } from "../input/ClickRecognizer";
import { ConnectController } from "../controller/ConnectController";

import { SecondEdgePainter } from "../canvas/EdgePainer";
import { GridBackgroundPainter } from "../canvas/BackgroundPainter";
import { EdgeHoverController } from "../controller/EdgeHoverController";

export interface ViewProps {
  data: IViewState;
  actions: {
    view: IViewActions;
    connectController: ConnectController.Actions;
  },
  queries: IViewQueries;
}

export class View extends React.Component<ViewProps> {

  componentDidMount() {
    this.ctx = this.canvas.getContext("2d");

    // initial canvas resize
    this.canvas.width = this.view.clientWidth;
    this.canvas.height = this.view.clientHeight;
    this.renderCanvas(this.props);

    // install resize observer
    this.resizeObserver = new ResizeObserver((entries) => {
      this.canvas.width = entries[0].contentRect.width;
      this.canvas.height = entries[0].contentRect.height;
      this.renderCanvas(this.props);
    });
    this.resizeObserver.observe(this.view);

    const domNode: Element = ReactDOM.findDOMNode(this) as any;
    this.controller = new ConnectController(domNode, this.props.data.controllers.connect, this.props.actions.connectController);
    this.hoverController = new EdgeHoverController(domNode, this.props.data, this.props.actions.view, this.props.queries);
  }

  componentWillUnmount() {
    this.resizeObserver.disconnect();
  }

  componentWillReceiveProps(next: ViewProps) {
    this.controller.updateState(next.data.controllers.connect, next.actions.connectController);
    this.hoverController.updateState(next.data, next.actions.view);
    this.renderCanvas(next);
  }

  render() {
    const style: React.CSSProperties = {
      width: `100%`, 
      height: `100%`,
      overflow: `hidden`,
      outline: `none`
    }

    const nodes = this.props.data.nodes;

    const onConnectorClick = (id: ConnectorId, connectorType: "in" | "out", button: MouseButton) => {
      if (this.controller) {
        this.controller.handleConnectorClick(id, connectorType, button);
      }
    }

    return (
    <div ref="view" className="node-view" tabIndex={0} style={style}>
      { 
        Array.from(nodes, ([id, n]) => 
          <Node key={id} node={n} connectors={this.props.data.connectors} actions={this.props.actions.view} onConnectorClick={onConnectorClick} />
        )
      }

      <canvas ref="canvas" />
    </div>);
  }

  private renderCanvas(props: ViewProps) {
    const ctx = this.ctx;
    const connectors = props.data.connectors;
    const hoverEdge = props.data.controllers.edgeHover.edge;

    this.backgroundPainter.render(ctx, this.canvas.width, this.canvas.height);

    this.edgePainter.begin(ctx);

    this.props.data.edges.forEach( edge => {
      const begin = connectors.get(edge.begin).position;
      const end   = connectors.get(edge.end).position;
      const beginDir = connectors.get(edge.begin).direction;
      const endDir = connectors.get(edge.end).direction;

      // skip edges that have no specified begin and end
      if (!begin || !end || !beginDir || !endDir) {
        return;
      }

      // scale the direction vectors by vertical length of the edge
      const d = imVec2.distance(begin, end);
      const lenBeginDir = vec2.length(beginDir);
      const lenEndDir = vec2.length(endDir);

      if (begin && end) {
        this.edgePainter.render(begin, end, 
          imVec2.scale(beginDir, Math.min(1.0, d / lenBeginDir)) ,
          imVec2.scale(endDir, Math.min(1.0, d / lenEndDir)),
          { highlighted: edge.id === hoverEdge, width: props.data.style.edgeWidth }
        );
      }
    });

    this.edgePainter.end();
  }

  private edgePainter = new SecondEdgePainter();
  private backgroundPainter = new GridBackgroundPainter();

  private controller: ConnectController;
  private hoverController: EdgeHoverController;

  private get canvas(): HTMLCanvasElement { return this.refs.canvas as HTMLCanvasElement; }
  private get view(): HTMLDivElement { return this.refs.view as HTMLDivElement; }

  private ctx: CanvasRenderingContext2D;
  private resizeObserver: ResizeObserver;
}