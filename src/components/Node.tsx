import * as React from "react";
import { Draggable } from "./Draggable";
import { DraggableHandle } from "./DraggableHandle";
import { vec2, imVec2 } from "../core/vec";
import { INode, IViewActions, IConnector, ConnectorId } from "../state/ViewStore";
import { Connector, ConnectorPosition } from "./Connector";
import { MouseButton } from "../input/ClickRecognizer";
import { ColorPicker, Saturation } from "./ColorPicker";
import { ColorResult } from "react-color";

export interface Properties {
  actions: IViewActions;
  node: INode;
  connectors: Map<ConnectorId, IConnector>

  onConnectorClick?: (connector: ConnectorId, connectorType: "in" | "out", button: MouseButton) => void;
}

function createConnectorStyle(col: number, row: number): React.CSSProperties {
  return {
    gridRow: row + 1,
    gridColumn: col + 1
  };
}

function gridContainerStyle(inCount: number, outCount: number): React.CSSProperties {
  const style = {
    display: "grid",
    gridGap: "7px 2em",
    paddingTop: "0.5em",
    paddingBottom: "1em"
  }

  if (inCount && outCount) {
    return { ...style,
      gridTemplateColumns: "auto auto"
    }
  }

  return style;
}

export const Node = (props: Properties) => {
  const dim = props.node.display.dimension;
  const position = props.node.display.position;
  const dimensionStyle: React.CSSProperties = {
    minWidth: dim[0],
    minHeight: dim[1]
  };
  const id = props.node.id;

  const callback = (type: "begin" | "update" | "end", pos: vec2) => {
    props.actions.moveNode(id, pos);
    return pos;
  };

  const positionCallback = (isInput: boolean, idx: number) => {
    return (connectorPos: imVec2) => {
      const offset = imVec2.subtract(connectorPos, position);
      props.actions.setNodeConnectorOffset(props.node.id, isInput, idx, offset)
    }
  };

  const clickCallback = (connectorId: ConnectorId, connectorType: "in" | "out", button: MouseButton) => {
    if (props.onConnectorClick) {
      props.onConnectorClick(connectorId, connectorType, button);
    }
  }

  const inputs = props.node.inputConnectors.map((ic, index) => { 
    const c = props.connectors.get(ic.connectorId);
    return {
      data: { ...ic, position: ConnectorPosition.Left, index, id: c.id },
      actions: props.actions, 
      onPositionChanged: positionCallback(true, index),
      onClick: (button: MouseButton) => clickCallback(ic.connectorId, "in", button)
    };
  });

  const outputs = props.node.outputConnectors.map((oc, index) => {
    const c = props.connectors.get(oc.connectorId);
    return {
      data: { ...oc, position: ConnectorPosition.Right, index, id: c.id },
      actions: props.actions, 
      onPositionChanged: positionCallback(false, index),
      onClick: (button: MouseButton) => clickCallback(oc.connectorId, "out", button)
    };
  });

  return <Draggable {...{position, callback}}>
    <div className="node" style={dimensionStyle} > 
      <DraggableHandle> <div className="title"> {props.node.title} </div> </DraggableHandle>
      {/* <ColorPicker>
        <Saturation onChange={undefined}/>
      </ColorPicker> */}
      <div style={gridContainerStyle(inputs.length, outputs.length)}>
          {  inputs.map( (data, idx) => <div key={idx} style={createConnectorStyle(0, idx)}> <Connector {...data} > </Connector> </div> ) }
          { outputs.map( (data, idx) => <div key={idx} style={createConnectorStyle(1, idx)}> <Connector {...data} > </Connector> </div> ) }
      </div>
    </div>
  </Draggable>
};