import * as React from "react";
import * as ReactDOM from "react-dom";

type DraggableCallback = (type: "begin" | "update" | "end", pos: number[]) => number[];

export interface DraggableProps {
  children?: any;
  position: Readonly<[number, number]>;
  callback?: DraggableCallback;
}

export interface DraggableState {
  position: number[]
}

export class Draggable extends React.Component<DraggableProps, DraggableState> {
  constructor(props: DraggableProps) {
    super(props);
    this.state = {
      position: [props.position[0], props.position[1]]
    };

    if (!props.callback) {
      props.callback = (type, pos) => pos;
    };
  }

  componentDidMount() {
    // find child nodes that act as draggable-handles
    const thisDOM = ReactDOM.findDOMNode(this) as Element;
    this.handles = thisDOM.getElementsByClassName("draggable-handle");

    for (let idx = 0; idx < this.handles.length; idx++) {
      this.handles.item(idx).addEventListener("mousedown", this.handleMouseDown.bind(this));
    }
  }

  render() {
    return <div className="draggable" style={this.positionStyle()}>
      {this.props.children}
    </div>
  }

  // internal stuff //////////////////////////////////////////////////////////////////////////

  private positionStyle(): React.CSSProperties {
    return {
      position: "absolute",
      left: this.state.position[0],
      top: this.state.position[1]
    }
  }

  private handleMouseDown(ev: React.MouseEvent<HTMLDivElement>) {
    this.latestX = ev.clientX;
    this.latestY = ev.clientY;

    document.onmousemove = this.handleMouseMove.bind(this);
    document.onmouseup = this.handleMouseUp.bind(this);

    const pos = this.state.position;

    this.setState({
      position: this.props.callback("begin", this.state.position)
    })
  }

  private handleMouseMove(ev: MouseEvent) {
    const pos = this.state.position;
    // The event coordinates depend on where the Draggable was clicked. 
    // To work around this, we use relative movement.
    const dx = ev.clientX - this.latestX;
    const dy = ev.clientY - this.latestY;
    this.latestX = ev.clientX;
    this.latestY = ev.clientY;

    this.setState({
      position: this.props.callback("update", [pos[0] + dx, pos[1] + dy])
    })
  }

  private handleMouseUp(ev: MouseEvent) {
    document.onmousemove = null;
    document.onmouseup = null;

    this.setState({
      position: this.props.callback("end", this.state.position)
    })
  }

  private handles: NodeListOf<Element>;

  private latestX: number;
  private latestY: number;
}